
import { LitElement, html, css} from 'lit-element';

class CambioPropiedades extends LitElement {

  // Cualquier cambio en las propiedades definidas en las propiedades estáticas desencadenará una nueva renderización
  // del componente.
  static get properties() {
    return {
      count: { type: Number },
    };
  }

  constructor() {
    super();
    this.count = 0;

    // Simulamos actualizar la propiedad de recuento cada 2 segundos, pasando de 0 a 10.
    setInterval(() => {
      this.count = this.count < 10 ? this.count + 1 : 0;
    }, 2000);
  }

  // La devolución de llamada _render se llama cada vez que cambia alguna de las propiedades definidas.
  // lit-html está optimizado para manejar actualizaciones frecuentes y actualizar el DOM de manera eficiente
  render() {
    return html`
      <div>
        Cuenta desde 0 hasta 10: [${this.count}]
      </div>
    `;
  }
}

customElements.define('cambio-propiedades', CambioPropiedades);