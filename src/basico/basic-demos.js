import { LitElement, html, css } from 'lit-element';
import './01-basic-setup.js';
import './02-manejando-propiedades.js';
import './03-cambio-propiedades.js';
import './04-propiedades-atributos.js';
import './05-pasando-propiedades.js';
import './06-manejando-eventos.js';
import './07-renderizado-condicional.js';
import './08-repitiendo-templates.js';
import './09-actualizando-objetos-arreglos.js';
import './10-renderizando-estilos.js';
import './11-buscando-informacion.js';
import './12-lanzando-eventos.js';

class BasicDemos extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: flex;
          flex-direction: column;
          font-family: sans-serif;
        }

        a {
          text-decoration: none;
        }

        .demo > *:not(h2):not(a) {
          display: block;
          border: 1px solid	#e2e2e2;
          border-radius: 5px;
          padding: 8px;
          margin: 8px 0;
          line-height: 32px;
        }

        paper-card { 
          border-radius: 5px;
          flex: 1; 
          padding: 12px;
          margin: 0 0 32px 0;
        }

        h2 {
          font-size: 20px;
          color: #2c3e50;
        }

        h2:hover::after { 
          color: #9B35FA;
          content: " #";
        }

        h1 {
          margin-top: 0px;
          color: #9B35FA;
        }
      `,
    ];
  }

  render() {
    return html`
      <paper-card>
        <div class="demo">
          <h2>01 Lo basico</h2></a>
          <basic-setup></basic-setup>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>02 Manejando propiedades</h2></a>
          <manejando-propiedades message="Hola mundo"></manejando-propiedades>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>03 Cambio de propiedades</h2></a>
          <cambio-propiedades></cambio-propiedades>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>04 Propiedades y atributos</h2></a>
          <propiedades-atributos message="Hola mundo!"></propiedades-atributos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>05 Pasando propiedades</h2></a>
          <pasando-propiedades></pasando-propiedades>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>06 Manejando eventos</h2></a>
          <manejando-eventos></manejando-eventos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>07 Renderizado condicional</h2></a>
          <renderizado-condicional message="Hola mundo!"></renderizado-condicional>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>08 Repitiendo templates</h2></a>
          <repitiendo-templates ></repitiendo-templates>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>09 Actualizando objetos y arreglos</h2></a>
          <actualizando-objetos-arreglos ></actualizando-objetos-arreglos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>10 Renderizando estilos</h2></a>
          <renderizando-estilos ></renderizando-estilos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>11 Buscando informacion</h2></a>
          <buscando-informacion ></buscando-informacion>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>12 Lanzando eventos</h2></a>
          <eventos-padre ></eventos-padre>
        </div>
      </paper-card>
    `;
  }
}

customElements.define('basic-demos', BasicDemos);