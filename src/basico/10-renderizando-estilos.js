import { LitElement, html, css } from 'lit-element';

class RenderizandoEstilos extends LitElement {
  /**
   *
   * Los estilos deben agregarse como un getter estático. Se evalúan una vez y luego se agregan
   * en el shadow dom del elemento.
   *
   * Shadow dom se encarga de analizar el CSS del elemento para afectar solo a su
   * plantilla del elemento, y no el elemento exterior.
   */
  static get styles() {
    return css`
      :host {
        display: block;
      }

      .message {
        color: blue;
      }
    `;
  }

  render() {
    return html`
      <div class='message'>Hello world!</div>
    `;
  }

}

customElements.define('renderizando-estilos', RenderizandoEstilos);