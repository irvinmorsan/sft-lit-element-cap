
import { LitElement, html, css} from 'lit-element';

const MESSAGE_PREFIX = 'El mensaje es: ';

class PropiedadesAtributos extends LitElement {

  static get properties() {
    return {
      href: { type: String },
      label: { type: String },
      message: { type: String },
      checked: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.href = 'https://bbva.es';
    this.label = 'BBVA';
    this.checked = true;

    // this.message se establece como atributo en ejercicios-clase-lit-basico-page.js`.
    // <propiedades-atributos message="Hola mundo!"></propiedades-atributos>
  }

  /**
  * En lit-html, pueden usar template expressions (${}) para establecer propiedades y atributos
  * en elementos con una sintaxis similar.
  *
  * La elección entre cuándo establecer una propiedad o un atributo depende del elemento. Como
  * una regla general siempre establece propiedades, solo establece atributos cuando tienes
  * valores estáticos que no cambian, o cuando es necesario establecer un atributo para un
  * tipo de elemento.
  */

  /**
  * Diferencias entre atributos y propiedades:
  *
  * Los atributos se definen en HTML, por ejemplo, estableciendo el atributo id:
  * <div id = "foo">
  *
  * Las propiedades se definen en javascript, por ejemplo, estableciendo la propiedad id:
  * const div = document.createElement ('div');
  * div.id = 'foo';
  */
  render() {
    return html`
      <!-- Por defecto, lit-html establecerá un atributo en el elemento-->
      <a href=${this.href}>
        ${this.label}
      </a>

      <!-- Podemos establecer atributos booleanos con el prefijo de signo de interrogación al nombre del atributo -->
      <input type='checkbox' ?checked=${this.checked} />

      <!--
        Para establecer una propiedad, anteponemos con un '.'
        Las propiedades distinguen entre mayúsculas y minúsculas(case-sensitive)
      -->
      <fancy-message 
        .messagePrefix=${MESSAGE_PREFIX} 
        .message=${this.message}>
      </fancy-message>

      <!--
        Si los valores son constantes, no necesitamos hacer un binding a la propiedad,
        podemos establecer el atributo directamente en el html

        HTML no distingue entre mayúsculas y minúsculas. Por defecto, LitElement usa el nombre de
        la propiedad en minúsculas como atributo
      -->
      <fancy-message 
        messageprefix='El mensaje es: ' 
        .message=${this.message}>
      </fancy-message>
    `;
  }
}

customElements.define('propiedades-atributos', PropiedadesAtributos);

class FancyMessage extends LitElement {
  static get properties() {
    return {
      message: { type: String },
      messagePrefix: { type: String }
    };
  }

  render() {
    return html`
      <div>${this.messagePrefix} ${this.message}</div>
    `;
  }
}

customElements.define('fancy-message', FancyMessage);