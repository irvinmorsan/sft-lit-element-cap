
import { LitElement, html, css} from 'lit-element';

class ActualizandoObjetosArreglos extends LitElement {
  static get properties() {
    return {
      myArray: { type: Array },
      myObject: { type: Object }
    };
  }

  constructor() {
    super();
    this.myObject = { id: 1, text: 'foo' };
    this.myArray = [{ id: 1 }, { id: 2 }];
  }

  render() {
    return html`
      <h3>Elementos Arreglo</h3>
      <ul>
        ${this.myArray.map(item => html`
          <li>${item.id}</li>
        `)}
      </ul>

      <button @click=${this._addArrayItem}>Adiciona un item al arreglo</button>

      <h3>Objecto</h3>
      <div>
        <strong>${this.myObject.id}</strong>: ${this.myObject.text}
      </div>
      
      <button @click=${this._updateObjectId}>Adiciona un item al objeto</button>
    `;
  }

  /**
   *
   * Si mutamos un arreglo directamente, LitElement no detectará
   * el cambio de forma automática.
   *
   * El enfoque recomendado es utilizar patrones de datos inmutables.
   * Podemos agregar fácilmente un elemento de matriz utilizando (el operador spread):
   */
  _addArrayItem() {
    const newId = Math.round(Math.random() * 100);
    const newItem = { id: newId };
    this.myArray = [
      ...this.myArray,
      newItem,
    ];

    /**
     * An alternative method is to mutate the array and then call
     * requestUpdate() to notify LitElement the property changed.
     */
    // this.myArray.push(newItem);
    // this.requestUpdate();
  }

  /**
   *
   * Si mutas un objeto directamente, LitElement no detectará
   * el cambio de forma automática.
   *
   * El enfoque recomendado es utilizar patrones de datos inmutables.
   * Puede actualizar fácilmente la propiedad de un objeto usando el operador spread
   */
  _updateObjectId() {
    const newId = Math.round(Math.random() * 100);

    this.myObject = {
      ...this.myObject,
      id: newId,
    };
    /**
     * An alternative method is to mutate the object and then call
     * requestUpdate() to notify LitElement the property changed.
     */
    // this.myObject.id = newId;
    // this.requestUpdate();
  }
}

customElements.define('actualizando-objetos-arreglos', ActualizandoObjetosArreglos);