import { LitElement, html, css } from 'lit-element';

class EventosPadre extends LitElement {
  someCallback(event) {
    alert(event.detail);
  }

  render() {
    return html`
      <eventos-hijo @event-fired=${this.someCallback}></eventos-hijo>
    `;
  }
}

customElements.define('eventos-padre', EventosPadre);

class EventosHijo extends LitElement {
  handleClick() {
    this.dispatchEvent(new CustomEvent('event-fired', { detail: 'Hola soy tu hijo!' }));
  }

  render() {
    return html`<button @click=${this.handleClick}>click hijo</button>`;
  }
}

customElements.define('eventos-hijo', EventosHijo);