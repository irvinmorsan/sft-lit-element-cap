
import { LitElement, html, css} from 'lit-element';

class ManejandoEventos extends LitElement {
  static get properties() {
    return {
      count: { type: Number },
    };
  }

  constructor() {
    super();

    this.count = 0;
  }

  render() {
    return html`
      <div>
        Conteo actual: [${this.count}]
        <!-- Utiliza la sintaxis @[eventname] para registrar de forma declarativa controladores de eventos en línea -->

        <button @click=${() => this.count += 1}>+</button>

        <!--
          También podemos pasar la referencia de una función directamente. Lit-html usará automáticamente el elemento
          como el 'scope' de la función ('this' hará referencia al elemento)
        -->
        <button @click=${this._onDecrement}>-</button>
      </div>
    `;
  }

  _onDecrement() {
    this.count -= 1;
  }
}

customElements.define('manejando-eventos', ManejandoEventos);