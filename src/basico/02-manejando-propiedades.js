
import { LitElement, html, css} from 'lit-element';

class ManejandoPropiedades extends LitElement {

  // Las propiedades se definen mediante un getter estático. Cuando se define, LitElement decodificará cualquier
  // atributos html establecidos en este componente como propiedades que se pueden utilizar desde javascript.

  // Las keys deben ser el nombre de la propiedad, valor el tipo de propiedad.
  // El type se utiliza para deserializar la cadena de atributos html en una propiedad de JavaScript.
  // Los tipos admitidos son String, Number, Boolean, Array Object.
  static get properties() {
    return {
      message: { type: String },
      count: { type: Number },
    };
  }

  constructor() {
    super();

    // los valores predeterminados se pueden establecer desde el constructor
    this.count = 0;
  }

  render() {
    return html`
      <div>
        <!--
          Las partes dinámicas de la plantilla se establecen mediante template strings.
          Es javascript simple, por lo que puede usar cualquier expresión de JavaScript válida. lit-html
          actualiza el dom de manera eficiente.
        -->

        <!-- Renderiza una cadena o un número directamente -->
        <div>
          El mensaje es: ${this.message}, count es: ${this.count}
        </div>

        <!-- Llama a una función y regresa el valor -->
        <div>
          El mensaje reservado: ${this.reverseMessage(this.message)}
        </div>

      </div>
    `;
  }

  reverseMessage(message) {
    return message.split('').reverse().join('');
  }
}

customElements.define('manejando-propiedades', ManejandoPropiedades);