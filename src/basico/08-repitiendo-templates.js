
import { LitElement, html } from 'lit-element';

class RepitiendoTemplates extends LitElement {
  static get properties() {
    return {
      libros: { type: Array },
    };
  }

  constructor() {
    super();

    this.libros = [
      { autor: 'G.R.R. Martin', titulo: 'A Game of Thrones'},
      { autor: 'Tolkien', titulo: 'Lord of the Rings'}
    ];
  }

  render() {
    return html`
      <!--
        Para repetir una plantilla, simplemente podemos usar la función map de JS.
        En este caso, asigna el arreglo de mensajes a un arreglo de plantillas.
        Lit-html leerá el arreglo y renderizará las plantillas dentro de ella.
      -->
      Libros:
      <ul>
        ${this.libros.map(libro => html`
          <li>${libro.autor}: ${libro.titulo}</li>
        `)}
      </ul>

      <!-- Si una plantilla se vuelve demasiado grande, también puede dividirla en una función separada -->
      Libros:
      <ul>
        ${this.libros.map(this._bookTemplate)}
      </ul>
    `;
  }

  _bookTemplate(book) {
    return html`<li>${book.autor}: ${book.titulo}</li>`;
  }
}

customElements.define('repitiendo-templates', RepitiendoTemplates);