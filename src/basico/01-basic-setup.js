// LitElement and html are the basic required imports
import { LitElement, html } from 'lit-element';

// Crea una definición de clase para su componente y amplíe la clase base LitElement
class BasicSetup extends LitElement {

  // el callback devuelve la renderización de la plantilla de tu elemento(clase). Esta debería ser una función pura,
  // siempre debe devolver la misma plantilla dadas las mismas propiedades. No debería funcionar
  // cualquier realizar un efecto secundario, como establecer propiedades o manipular el DOM.
  render() {

    // Devuelve la plantilla usando la etiqueta de plantilla html. lit-html analizará la plantilla y
    // crea los elementos DOM
    return html`
      <div>Hola mundo!</div>
    `;
  }
}

// Registramos su elemento con customElements.define, le pasamos un nombre de etiqueta y la definición de clase
// El nombre del elemento siempre debe contener al menos un guión

customElements.define('basic-setup', BasicSetup);