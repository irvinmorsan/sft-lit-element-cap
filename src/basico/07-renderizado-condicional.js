
import { LitElement, html} from 'lit-element';

// You also implement conditional logic in separate functions
function getMessage(message, showMessage) {
  if (!showMessage) {
    return '';
  }
  return `Mensaje desde la función: ${message}`;
}

class RenderizadoCondicional extends LitElement {

  static get properties() {
    return {
      showMessage: { type: Boolean },
      message: {type: String},
      disabled: { type: Boolean },
    };
  }

  render() {
    // podemos usar declaraciones if regulares
    if (this.disabled) {
      return html`Nothing to see here`;
    }

    return html`
      <div>
        <button @click=${() => this.showMessage = !this.showMessage}>
          <!-- Podemos utilizar expresiones ternarias para una representación condicional rápida -->
          Haz click para ${this.showMessage ? 'ocultar' : 'mostrar'} el mensaje
        </button>

        <!-- O para mostrar / ocultar condicionalmente una plantilla -->
        <div>
          ${this.showMessage ? html`El mensaje es: ${this.message}` : ''}
        </div>

        <!-- También podemos llamar a una función y manejar(handle) la renderización ahí -->
        <div>
          ${getMessage(this.message, this.showMessage)}
        </div>
      </div>
    `;
  }
}

customElements.define('renderizado-condicional', RenderizadoCondicional);