
import { LitElement, html, css} from 'lit-element';

class PasandoPropiedades extends LitElement {

  static get properties() {
    return {
      author: { type: Object },
      books: {type: Array}
    };
  }

  constructor() {
    super();
    this.author = { name: 'G.R.R. Martin', age: 70 };
    this.books = [
      { title: 'Game of Thrones', pages: 697 },
      { title: 'The Ice Dragon', pages: 521 }
    ];
  }

  /**
   * En lit-html, podemos usar template expressions($ {}) para establecer propiedades y atributos
   * en elementos con una sintaxis similar.
   */
  render() {
    return html`
      <perfil-autor
        .author=${this.author}
        .books=${this.books}>
      </perfil-autor>
    `;
  }

}

customElements.define('pasando-propiedades', PasandoPropiedades);

class PerfilAutor extends LitElement {
  static get properties() {
    return {
      author: { type: Object },
      books: { type: Array },
    };
  }

  render() {
    return html`
      <div>${this.author.name}</div>
      <div>${this.books.map(book => html`<li>${book.title}</li>`)}</div>
    `;
  }
}

customElements.define('perfil-autor', PerfilAutor);