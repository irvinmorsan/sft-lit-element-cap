import { LitElement, html, css } from 'lit-element';

// A powerful feature of lit-html is that templates are just variables.
// You can create them anywhere, and you can define generic templates
// to be used by multiple components.

// This template could be defined in a separate file

// Una característica poderosa de lit-html es que los plantillas o templates son solo variables.
// Se pueden crear desde cualquier lugar y se pueden definir plantillas o templates genéricas
// para ser utilizado por múltiples componentes.

// Esta plantilla podría definirse en un archivo separado
const sharedTemplate = html`
  <div>Shared template content</div>
`;

class SharedTemplateA extends LitElement {
  static get styles() {
    return css`
      .container {
        border: 2px solid black;
      }
    `;
  }

  render() {
    return html`
    <!-- Este elemento usa la plantilla compartida -->
      <div class="container">
        Element A
        ${sharedTemplate}
      </div>
    `;
  }

}

customElements.define('shared-template-a', SharedTemplateA);

class SharedTemplateB extends LitElement {
  static get styles() {
    return css`
      .container {
        border: 2px dotted black;
      }
    `;
  }

  render() {
    return html`
      <!-- Este elemento usa la plantilla compartida -->
      <div class="container">
        Element B
        ${sharedTemplate}
      </div>
    `;
  }

}

customElements.define('shared-template-b', SharedTemplateB);

class TemplatesCompartidos extends LitElement {
  static get styles() {
    return css`
      shared-template-a,
      element-b {
        display: block;
        margin: 8px 0;
      }
    `;
  }

  render() {
    return html`
      <shared-template-a></shared-template-a>
      <shared-template-b></shared-template-b>
    `;
  }

}

customElements.define('templates-compartidos', TemplatesCompartidos);