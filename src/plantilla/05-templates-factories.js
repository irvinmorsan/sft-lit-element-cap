import { LitElement, html } from 'lit-element';

// Aquí definimos una plantilla que como una función acepta un conjunto de variables
// y devuelve la plantilla html. Esto es en realidad idéntico a cómo el render de
// la función funciona.
//
// Esta función podría definirse en un archivo separado, compartido o simplemente ser una buena
// forma de dividir una plantilla o template más grande. También se puede utilizar para crear componentes diminutos.
// que no necesitan mucho aislamiento, podemos crear una función para eso en lugar de un componente web.
//
// En este caso aceptamos una propiedad, contenido de texto y un controlador de eventos
const templateFactory = (inputValue, buttonText, onSubmit) => html`
  <input id="usernameInput" value="${inputValue}">

  <button @click="${onSubmit}">
    ${buttonText}
  </button>
`;

class TemplateFactories extends LitElement {

  static get properties() {
    return {
      username: { type: String },
    };
  }

  constructor() {
    super();

    this.username = 'Steve';
    this._onUsernameSubmit = this._onUsernameSubmit.bind(this);
  }

  render() {
    return html`
      <!--
        Llamamos a la fábrica de plantillas o templates con una propiedad, una cadena constante
        y una referencia al método para el controlador de eventos
      -->
      <h3>Usuario: ${this.username}</h3>
      ${templateFactory(this.username, 'Cambia el usuario', this._onUsernameSubmit)}
    `;
  }

  _onUsernameSubmit() {
    this.username = this.shadowRoot.getElementById('usernameInput').value;
  }

}

customElements.define('template-factories', TemplateFactories);
