import { LitElement, html, css } from 'lit-element';
import './01-observador-setter-propiedades';
import './02-template-wrapping';
import './03-templates-compartidos';
import './04-estilos-compartidos';
import './05-templates-factories';
import './06-should-update';

class AdvancedDemos extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: flex;
          flex-direction: column;
          font-family: sans-serif;
        }

        a {
          text-decoration: none;
        }

        .demo > *:not(h2):not(a) {
          display: block;
          border: 1px solid	#e2e2e2;
          border-radius: 5px;
          padding: 8px;
          margin: 8px 0;
          line-height: 32px;
        }

        paper-card { 
          border-radius: 5px;
          flex: 1; 
          padding: 12px;
          margin: 0 0 32px 0;
        }

        h2 {
          font-size: 20px;
          color: #2c3e50;
        }

        h2:hover::after { 
          color: #9B35FA;
          content: " #";
        }

        h1 {
          margin-top: 0px;
          color: #9B35FA;
        }
      `,
    ];
  }


  render() {
    return html`
      <paper-card>
        <div class="demo">
          <h2>01 Observador setter propiedades</h2></a>
          <observador-setter-propiedades></observador-setter-propiedades>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>02 Template wrapping</h2></a>
          <template-wrapping></template-wrapping>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>03 Templates compartidos</h2></a>
          <templates-compartidos></templates-compartidos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>04 Estilos compartidos</h2></a>
          <estilos-compartidos></estilos-compartidos>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>05 Template factories</h2></a>
          <template-factories></template-factories>
        </div>
      </paper-card>

      <paper-card>
        <div class="demo">
          <h2>06 Should update</h2></a>
          <should-update></should-update>
        </div>
      </paper-card>
    `;
  }
}

customElements.define('advanced-demos', AdvancedDemos);