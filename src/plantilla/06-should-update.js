import { LitElement, html } from 'lit-element';

class ShouldUpdate extends LitElement {

  static get properties() {
    return {
      active: { type: Boolean },
      count: { type: Number },
    };
  }

  constructor() {
    super();

    this.active = true;
    this.count = 0;

    // Incrementa un contador cada segundo. Tener en cuenta que incluso cuando se impide el procesamiento, JavaScript
    // continua funcionando. Cuando la renderización se vuelve a activar en un momento posterior, los valores de
    // las propiedades en ese momento se recuperan nuevamente.
    setInterval(() => {
      this.count += 1;
    }, 1000);
  }

  render() {
    return html`
      Current count: ${this.count}
      <button @click="${() => this.active = !this.active}">
        Toogle
      </button>
    `;
  }

  // ${console.log(this.active)}

  // La función shouldUpdate recibe un mapa con claves para las propiedades modificadas
  // apuntando a sus valores anteriores. Basado en esto, podemos decidir si el componente
  // debe volver a renderizarse o no. Esto es útil para bloquear la reproducción
  // del componente mientras no está activo o a la vista.
  shouldUpdate(changedProperties) {
    return this.active;
  }

}

customElements.define('should-update', ShouldUpdate);