import { LitElement, html } from 'lit-element';

// Debido a que las plantillas son solo variables, podemos pasarlas en funciones.
// Según determinadas condiciones las podemos usar.

function wrapItemTemplate(item, template) {
  switch (item.type) {
    case 'button':
      return html`<button>${template}</button>`;
    case 'link':
      return html`<a href="${item.url}">${template}</a>`;
    default:
      return template;
  }
}

// Create the custom template for each item
function getItemTemplate(item) {
  // Crea la plantilla personalizada para cada artículo
  const baseTemplate = html`<div>${item.message}</div>`;

  // Llama y devuelve el valor de las funciones de los factory functions
  return wrapItemTemplate(item, baseTemplate);
}

class TemplateWrapping extends LitElement {

  static get properties() {
    return {
      items: { type: Array },
    };
  }

  constructor() {
    super();

    this.items = [
      { type: 'button', message: 'hola' },
      { type: 'link', url: '#!', message: 'mundo' },
      { type: 'none', message: 'foo' },
    ];
  }

  render() {
    return html`
      <!-- Asigna cada elemento a su propia plantilla -->
      ${this.items.map(getItemTemplate)}
    `;
  }

}

customElements.define('template-wrapping', TemplateWrapping);