import { LitElement, html } from 'lit-element';

class ObservadorSetterPropiedades extends LitElement {

  static get properties() {
    return {
      _focused: { type: Boolean },
    };
  }

  constructor() {
    super();

    this.focused = false;
  }

  get focused() {
    return this._focused;
  }

  // Para observar los cambios de propiedad, podemos escribir nuestros propios getters y setters
  // y crear efectos secundarios cuando se 'setteen'. Cuando las propiedades son 'setteadas' o afectadas
  // por otros componentes durante el renderizado, se llamará al setter.

  set focused(focused) {

    // Si además de los efectos secundarios, también necesitamos el valor para ser renderizado que se
    // puede establecer la propiedad con un nombre diferente.
    this._focused = focused;

    if (focused) {
      this.shadowRoot.getElementById('amountInput').focus();
    }
  }

  render() {
    return html`
      <input id="amountInput" type="number" name="amount" @blur="${() => this.focused = false}">
      Focused: ${this._focused}
      <button @click="${() => this.focused = true}">Focus input</button>
    `;
  }

}

customElements.define('observador-setter-propiedades', ObservadorSetterPropiedades);