import { LitElement, html, css } from 'lit-element';

// Debido a que las etiquetas de estilos son solo html, se pueden compartir de la misma manera
// que se comparten las plantillas regulares.

// Esta plantilla podría definirse en un archivo separado
const sharedStyles = css`
  :host {
    display: block;
  }

  .box-wrapper {
    border: 5px solid red;
  }

  .box {
    width: 36px;
    height: 36px;
    background-color: blue;
  }
`;

class SharedStylesA extends LitElement {
  static get styles() {
    return sharedStyles;
  }

  render() {
    return html`
      ${sharedStyles}
      <div class="box-wrapper">
        <div class="box"></div>
      </div>
    `;
  }
}

customElements.define('shared-styles-a', SharedStylesA);

class SharedStylesB extends LitElement {
  static get styles() {

    // para componer estilos, devolvemos un arreglo
    return [

      // extraemos los estilos compartidos
      sharedStyles,

      // podemos agregar estilos propios
      css`
      /* Puede anular o 'override' las clases definidas en estilos compartidos */
      .box-wrapper {
        border: 5px dotted green;
      }
    `];
  }

  render() {
    return html`
      <div class="box-wrapper">
        <div class="box"></div>
      </div>
    `;
  }
}

customElements.define('shared-styles-b', SharedStylesB);

class EstilosCompartidos extends LitElement {
  static get styles() {
    return css`
      shared-styles-a,
      shared-styles-b {
        display: inline-block;
        margin: 8px 0;
      }
    `;
  }

  render() {
    return html`
      <shared-styles-a></shared-styles-a>
      <shared-styles-b></shared-styles-b>
    `;
  }
}

customElements.define('estilos-compartidos', EstilosCompartidos);